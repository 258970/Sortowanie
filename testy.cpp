#include "Sortowanie.h"
#include <iostream>
#include <math.h>
#include <chrono>

template<typename T>
void killTable(T *table){
    delete[] table;
}

template<typename T>
void wypelnijLosowo(T *arr, int n){
    for(int i = 0; i<n; i++)
        arr[i] = rand() % 1000 + 1;
}


template<typename T>
void sortujProcenty(T *arr, int n, double percentage){
    if(percentage == 100){
        std::sort(arr, arr + n, std::greater<T>());
        return;
    }
    std::sort(arr, arr + (int)((n*percentage) / 100));
}


template<typename T>
void sprawdzCzyPosortowane(T *arr, int n){
    for(int i=0; i < n-1; i++){
        if(arr[i] > arr[i+1])
            std::cout << "Nieposortowane \n Zamykanie programu.."<<std::endl;
    }
}

template<typename T>
double sortMerge(T* arr, int n){
    auto t_start = std::chrono::high_resolution_clock::now();
    mergeSort(arr, 0, n-1);
    auto t_end = std::chrono::high_resolution_clock::now();
    return std::chrono::duration<double, std::milli>(t_end-t_start).count();
}

template<typename T>
double sortQuick(int n, T *arr){
    auto t_start = std::chrono::high_resolution_clock::now();
    quickSort(arr, 0, n-1);
    auto t_end = std::chrono::high_resolution_clock::now();
    return std::chrono::duration<double, std::milli>(t_end-t_start).count();
}

template<typename T>
double sortIntro(T *arr, int x, int max){
    auto t_start = std::chrono::high_resolution_clock::now();
    introspectiveSort(arr, arr, arr+x, max);
    auto t_end = std::chrono::high_resolution_clock::now();
    return std::chrono::duration<double, std::milli>(t_end-t_start).count();
}


int main(){
    srand(time(NULL));

    int iloscPowtorzen=1;
    int iloscElementow[5] = {10000, 50000, 100000, 500000, 1000000};
    //int iloscElementow[5] = {2, 3,4,5,6};
    double percentagearr[8] = {0, 25, 50, 75, 95, 99, 99.7, 100};

    double licznik = 0;

    std::cout<< "Merge Sort" << std::endl;
    for(double percentage : percentagearr){
        for(int n : iloscElementow){
            int *arr = new int[n];

            for(int i = 0; i<iloscPowtorzen; i++){
                wypelnijLosowo<int>(arr, n);
                sortujProcenty<int>(arr, n, percentage);
                licznik += sortMerge<int>(arr, n);
                sprawdzCzyPosortowane<int>(arr, n);

                std::cout << i << "%" << "\r";
            }
            std::cout << licznik/iloscPowtorzen << " ";
            licznik = 0;
            killTable<int>(arr);

            std::cout<< n << " Tablica posortowana" << std::endl;
        }
        std::cout<< percentage <<" % zrobione" << std::endl;
    }


    std::cout<< "Quick Sort" << std::endl;
    for(double percentage : percentagearr){
        for(int n : iloscElementow){
            int *arr = new int[n];

            for(int i = 0; i<iloscPowtorzen; i++){
                wypelnijLosowo<int>(arr, n);
                sortujProcenty<int>(arr, n, percentage);
                licznik += sortQuick<int>(n, arr);
                sprawdzCzyPosortowane<int>(arr, n);

                std::cout << i << "%" << "\r";
            }
            std::cout << licznik/iloscPowtorzen << " ";
            licznik = 0;
            killTable<int>(arr);

            std::cout<< n << " Tablica posortowana" << std::endl;
        }
        std::cout<< percentage <<" % zrobione" << std::endl;
    }

    std::cout<< "Introspective Sort" << std::endl;
    for(double percentage : percentagearr){
        for(int n : iloscElementow){
            int *arr = new int[n];

            for(int i = 0; i<iloscPowtorzen; i++){
                wypelnijLosowo<int>(arr, n);
                sortujProcenty<int>(arr, n, percentage);
                licznik += sortIntro<int>(arr, n-1, log(n)*2);
                sprawdzCzyPosortowane<int>(arr, n);

                std::cout << i << "%" << "\r";
            }
            std::cout << licznik/iloscPowtorzen << " ";
            licznik = 0;
            killTable<int>(arr);

            std::cout<< n << " Tablica posortowana" << std::endl;
        }
        std::cout<< percentage <<" % zrobione" << std::endl;
    }
    return 0;
}