#ifndef SORT_H
#define SORT_H

#include <algorithm>

template<typename T>
void mergeSort(T *arr, int left, int right);

template<typename T>
void quickSort(T *arr, int left, int right);

template<typename T>
void introspectiveSort(T *arr, T *left, T *right, int max);

#endif