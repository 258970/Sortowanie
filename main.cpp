#include <iostream>
#include "Sortowanie.h"

template <typename T>
void scalanie(T *arr, int start, int middle, int end){
    int i, j, k, nl, nr;
    nl = middle-start+1; nr = end-middle;           //Definiujemy rozmiary nowych tablic
    int larr[nl], rarr[nr];
    for(i = 0; i<nl; i++)
        larr[i] = arr[start + i];                   //Do lewej podtablicy przypisujemy wartości z głównej tablicy zaczynając od danego miejsca
    for(j = 0; j<nr; j++)
        rarr[j] = arr[middle+1+j];                  //Do prawej podtablicy przypisujemy wartości z głównej tablicy zaczynając od danego miejsca
    i=0; j=0; k=start;

    while (i<nl && j<nr){
        if(larr[i]<=rarr[j]){             //Jeśli element z prawej podtablicy jest równy bądz wiekszy od e. z lewej to
            arr[k]=larr[i];                 //przypisujemy do danego miejsca w głównej tablicy z e. z lewej podtablicy
            i++;
        }
        else{
            arr[k] = rarr[j];               //W przeciwnym wypadku przypisuje e. z prawej podtablicy
            j++;
        }
        k++;
    }
    while(i<nl){                            //Wpisanie pozostałych elementów do głównej tablicy jeśli takie zostały
        arr[k] = larr[i];
        i++; k++;
    }
    while(j<nr){                            //Wpisanie pozostałych elementów do głównej tablicy jeśli takie zostały
        arr[k]=rarr[j];
        j++; k++;
    }

}


template<typename T>
void mergeSort(T *arr, int start, int end){
    if(start<end){
        int middle = (end + start) / 2;             //Szukamy pozycji w połowie żeby podzielić tablice
        mergeSort(arr, start, middle);              //Dzielimy na pierwszą część podtablicy
        mergeSort(arr, middle + 1, end);            //Dzielimy na drugą część podtablicy
        scalanie(arr, start, middle, end);           //Łączymy podtablice w całość
    }
}



template<typename T>
int podziel(T *arr, int left, int right){
    int pivot = arr[(left + right) / 2], i = left, j = right;

    while(true){
        while(arr[j] > pivot)
            j--;
        while(arr[i] < pivot)
            i++;

        if(i<j)
            std::swap(arr[i++], arr[j--]);
        else
            return j;
    }
}

template<typename T>
void quickSort(T *arr, int left, int right){
    if(left<right){
        int q = podziel(arr, left, right);   //Dzielimy tablice
        quickSort(arr, left, q);             //Sortujemy lewą strone tablicy
        quickSort(arr, q+1, right);          //Sortujemy prawą strone tablicy
    }
}



template<typename T>
void insertionSort(T *arr, int left, int right){        //Sortowanie przez wstawianie
    for(int i = left+1; i <= right; i++){
        int klucz = arr[i];
        int j = i;

        while(j > left && arr[j-1] > klucz){        //Dopóki j jest większe od left i element jest większy od następnego
            arr[j] = arr[j-1];                      //Każdy Dodany element porównujemy z e. po jego lewej stronie idąc od prawej do lewej
            j--;                                    //Gdy okaze sie ze klucz jest mniejszy to przesuwamy e. z lewej o 1 w prawo, a klucz idzie w lewo
        }
        arr[j] = klucz;                    //Gdy z lewej jest mniejsza od klucza to przypisujemy do danego elementu
    }
}


template<typename T>
void heapSort(T *left, T *right){
    std::make_heap(left, right);
    std::sort_heap(left, right);
}



template<typename T>
void introspectiveSort(T *arr, T *left, T *right, int max){
    if((right-left)<16)                                                 //Jeżeli r. tablicy mniejszy od 16 to przez wstawianie
        insertionSort(arr, left - arr, right - arr);
    else if(max == 0)                                           //Jesli glebokosc rowna 0 to sortowanie przez kopcowanie
        heapSort(left, right + 1);
    else{                                                       //W przeciwnym wypadku intro sort
        int pivot = podziel(arr, left-arr, right-arr);
        introspectiveSort(arr, left, arr + pivot, max - 1);
        introspectiveSort(arr, arr + pivot + 1, right, max - 1);
    }
}




template void mergeSort<int>(int*, int, int);
template void quickSort<int>(int*, int, int);
template void introspectiveSort<int>(int*, int*, int*, int max);